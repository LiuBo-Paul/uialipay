//
//  AppDelegate.swift
//  UIAliPay
//
//  Created by Paul on 2018/11/13.
//  Copyright © 2018 LiuBo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.white
        window?.makeKeyAndVisible()
        
        let homeViewController = HomeViewController.init(nibName: "HomeViewController", bundle: nil);
        window?.rootViewController = homeViewController;
        
        return true
    }

}

