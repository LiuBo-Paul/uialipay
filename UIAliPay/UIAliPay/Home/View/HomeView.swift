//
//  HomeView.swift
//  UIAliPay
//
//  Created by Paul on 2018/11/14.
//  Copyright © 2018 LiuBo. All rights reserved.
//

import UIKit

class HomeView: UIView
{
    @IBOutlet var searchBtn: UIButton!
    @IBOutlet var voiceBtn: UIButton!
    @IBOutlet var friendBtn: UIButton!
    @IBOutlet var addBtn: UIButton!
    
    init(frame: CGRect, type: NSInteger)
    {
        super.init(frame: frame)
        self.frame = frame
        
        for view in self.subviews
        {
            view.removeFromSuperview();
        }
        let view = Bundle.main.loadNibNamed("HomeView", owner: self, options: nil)?[type] as! HomeView
        view.frame = frame;
        
        self.addSubview(view);
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        load_init()
    }
    func load_init(){
    }
}
