//
//  HomeBackScrollView.swift
//  UIAliPay
//
//  Created by Paul on 2018/11/14.
//  Copyright © 2018 LiuBo. All rights reserved.
//

import UIKit

class HomeBackScrollView: UIScrollView, UIGestureRecognizerDelegate {

    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return gestureRecognizer.isKind(of: UIPanGestureRecognizer.classForCoder()) && otherGestureRecognizer.isKind(of: UIPanGestureRecognizer.classForCoder());
    }

}
