//
//  HomeViewController.swift
//  UIAliPay
//
//  Created by Paul on 2018/11/13.
//  Copyright © 2018 LiuBo. All rights reserved.
//

import UIKit
import MJRefresh

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate
{
    @IBOutlet var top_headerView: UIView!
    @IBOutlet var search_headerView: UIView!
    @IBOutlet var mainBackScrollView: HomeBackScrollView!
    
    var mainTableView: UITableView!
    var dataArray: NSMutableArray!
    var mainHeaderBackView: UIView!
    var mainHeaderView: UIView!
    var subHeaderView: UIView!
    
    let mainHeaderViewHeight = 80;
    let subHeaderViewHeight = 240;
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        top_headerView.addSubview(HomeView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: top_headerView.frame.size.height), type: 0));
        search_headerView.addSubview(HomeView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: search_headerView.frame.size.height), type: 1));
        
        
        mainBackScrollView.delegate = self;
        mainBackScrollView.showsVerticalScrollIndicator = false
        
        mainBackScrollView.addObserver(self, forKeyPath: "contentOffset", options: [.new, .old], context: nil);
        
        mainHeaderBackView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: Int(UIScreen.main.bounds.size.width), height: mainHeaderViewHeight + subHeaderViewHeight));
        mainHeaderBackView.backgroundColor = UIColor.init(red: 30.0/255.0, green: 129.0/255.0, blue: 210.0/255.0, alpha: 1.0);
        
        mainHeaderView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: Int(UIScreen.main.bounds.size.width), height: mainHeaderViewHeight));
        mainHeaderView.addSubview(HomeView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: mainHeaderView.frame.size.height), type: 2));
        mainHeaderBackView.addSubview(mainHeaderView);
        
        subHeaderView = UIView.init(frame: CGRect.init(x: 0, y: mainHeaderViewHeight, width: Int(UIScreen.main.bounds.size.width), height: subHeaderViewHeight))
        subHeaderView.addSubview(HomeView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: subHeaderView.frame.size.height), type: 3));
        mainHeaderBackView.addSubview(subHeaderView);
        
        mainBackScrollView.addSubview(mainHeaderBackView);
        
        let headerHeight = mainHeaderBackView.frame.size.height + mainHeaderBackView.frame.origin.y;
        
        mainTableView = UITableView.init(frame: CGRect.init(x: 0, y: headerHeight + 10, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height), style: UITableView.Style.plain)
        mainTableView.delegate = self;
        mainTableView.dataSource = self;
        
        mainTableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            sleep(UInt32(1.5));
            self.mainTableView.mj_header.endRefreshing();
        });
        
        mainBackScrollView.addSubview(mainTableView);
        mainBackScrollView.contentSize = CGSize.init(width: UIScreen.main.bounds.size.width, height: mainTableView.frame.size.height + mainTableView.frame.origin.y)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if(mainBackScrollView.contentOffset.y < 0)
        {
            mainBackScrollView.isScrollEnabled = false;
        }
        else if(mainBackScrollView.contentOffset.y == 0)
        {
            mainBackScrollView.isScrollEnabled = true;
        }
        else
        {
            mainBackScrollView.isScrollEnabled = true;
            if(mainBackScrollView.contentOffset.y >= 0 && mainBackScrollView.contentOffset.y <= 60)
            {
                //顶部搜索栏和小控件操作栏切换alpha
                let pos = mainBackScrollView.contentOffset.y/60.0;
                top_headerView.alpha = pos;
                search_headerView.alpha = 1 - pos;
                mainHeaderView.alpha = 1 - pos;
            }
            else if(mainBackScrollView.contentOffset.y > 60)
            {
                top_headerView.alpha = 1;
                search_headerView.alpha = 0;
                mainHeaderView.alpha = 0;
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 50;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 148;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell");
        if(cell == nil)
        {
            cell = UITableViewCell.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell");
        }
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 1;
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if(scrollView.classForCoder == UITableView.classForKeyedArchiver())
        {
            if(mainTableView.contentOffset.y > 150)
            {
                mainBackScrollView.contentOffset = CGPoint.init(x: 0, y: mainHeaderBackView.frame.size.height + mainHeaderBackView.frame.origin.y)
            }
            else
            {
                if(mainBackScrollView.contentOffset.y < mainHeaderBackView.frame.size.height + mainHeaderBackView.frame.origin.y)
                {
                    mainBackScrollView.isScrollEnabled = true;
                    if(mainTableView.contentOffset.y > 0)
                    {
                        mainTableView.contentOffset = CGPoint.zero;
                    }
                }
                else
                {
                    mainBackScrollView.contentOffset = CGPoint.init(x: 0, y: mainHeaderBackView.frame.size.height + mainHeaderBackView.frame.origin.y)
                }
            }
        }
        else if(scrollView.classForCoder == HomeBackScrollView.classForKeyedArchiver())
        {
            
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>)
    {
        adjustMainBackScrollView();
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
        adjustMainBackScrollView();
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        adjustMainBackScrollView();
    }
    
    func adjustMainBackScrollView()
    {
        if(mainBackScrollView.contentOffset.y > 0 && (mainBackScrollView.contentOffset.y < CGFloat(mainHeaderViewHeight)/2.0))
        {
            self.mainBackScrollView.setContentOffset(CGPoint.zero, animated: true);
        }
        else if(mainBackScrollView.contentOffset.y >= CGFloat(mainHeaderViewHeight)/2.0 && mainBackScrollView.contentOffset.y <= CGFloat(mainHeaderViewHeight))
        {
            self.mainBackScrollView.setContentOffset(CGPoint.init(x: 0, y: CGFloat(mainHeaderViewHeight)), animated: true);
        }
        let pos = self.mainBackScrollView.contentOffset.y/CGFloat(mainHeaderViewHeight);
        self.top_headerView.alpha = pos;
        self.search_headerView.alpha = 1 - pos;
        self.mainHeaderView.alpha = 1 - pos;

        if(mainBackScrollView.contentOffset.y < 0)
        {
            mainBackScrollView.isScrollEnabled = false;
            mainBackScrollView.contentOffset = CGPoint.zero;
        }
    }
}
